# CI templates

## Usage example

These are example `.gitlab-ci.yml` files.

### Docker image build

```yaml
stages:
  - docker

# include CI config from another repo
include:
  - project: disappointment-industries/ci-templates
    file: /docker.yml

docker_build:
  extends: .docker
  variables:
    DESTINATION: "somewhere/in/the/project"
    IMAGE_NAME: "fancy/name"
    TAG: "definietly_not_latest"
```
### ssh deploy

```yaml
stages:
  - deploy
  
# include CI config from another repo
include:
  - project: disappointment-industries/ci-templates
    file: /deploy.yml

Deploy:
  extends: .deploy
  variables:
    SSH_KEY: "$MY_SSH_KEY"
    PROXY_JUMP: "proxy_server.tld"
    FILES: "magic.sh oopsie.yml something/other.go"
    HOSTS: "server1.tld server2.tld server4.tld"
    ### commands ###
    BEFORE_COPY: "cd ../../../../; echo hacked; mkdir -p haha/hehe/f"
    AFTER_COPY: "cd project/dir; docker-compose up -d; echo meh"
    ### copy destination ###
    DESTINATION: "my_awsome_app/files/anything"
    ### users ###
    PROXY_USER: "ci"
    BEFORE_USER: "ci"
    COPY_USER: "ci"
    AFTER_USER: "root"
```

### go build

```yaml
stages:
  - go_build
  
# include CI config from another repo
include:
  - project: disappointment-industries/ci-templates
    file: /go_build.yml

Deploy:
  extends: .go_build
  artifacts:
    paths:
      - app
      - static
```

enjoy!